﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;
using System.Runtime.ExceptionServices;
using System.Text.RegularExpressions;

using ThermoFisher.CommonCore.Data;
using ThermoFisher.CommonCore.Data.Business;
using ThermoFisher.CommonCore.Data.FilterEnums;
using ThermoFisher.CommonCore.Data.Interfaces;
using ThermoFisher.CommonCore.MassPrecisionEstimator;
using ThermoFisher.CommonCore.RawFileReader;

namespace Raw2Peaks
{
    class MyConfig
    {
        public List<string> filenames = new List<string>();
        public string filtertext = "FTMS + p NSI Full ms";
        public double snr = 10.0;
        public double thresh = 10000.0;
        public double ppm = 2.0;
        public int minpeakpoints = 5;
        public bool printall = false;

        public MyConfig(string[] args)
        {
            for (int i=0; i< args.Length; i++)
            {
                switch (args[i])
                {
                    case "--filtertext":
                        filtertext = args[++i];
                        break;
                    case "--thresh":
                        thresh = Double.Parse(args[++i]);
                        break;
                    case "--snr":
                        snr = Double.Parse(args[++i]);
                        break;
                    case "--ppm":
                        ppm = Double.Parse(args[++i]);
                        break;
                    case "--minpeakpoints":
                        minpeakpoints = int.Parse(args[++i]);
                        break;
                    case "--printall":
                        printall = true;
                        break;

                    default:
                        filenames.Add(args[i]);
                        break;
                }
            }
        }
    }
    class MzCentroid
    {
        public double rt;
        public double mz;
        public double ic;
        public MzCentroid(double rt, double mz, double ic)
        {
            this.rt = rt;
            this.mz = mz;
            this.ic = ic;
        }
    }
    class MzPeak
    {
        public double lastMz;
        public double lastRt;
        public double tic = 0;
        public List<MzCentroid> mzPeaks = new List<MzCentroid>();
        public MzPeak(double rt, double mz, double ic)
        {
            this.Add(rt, mz, ic);
        }
        public void Add(double rt, double mz, double ic)
        {
            lastRt = rt;
            lastMz = mz;
            mzPeaks.Add(new MzCentroid(rt, mz, ic));
            tic += ic;
        }
        public int CompareMZ(double mz, double ppm)
        {
            double delta = mz * ppm / 1000000;
            if(mz + delta < this.lastMz)
            {
                return -1;
            }
            if(mz - delta > this.lastMz)
            {
                return 1;
            }
            return 0;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            MyConfig config = new MyConfig(args);
            if(config.filenames.Count == 0)
            {
                PrintHelp();
                return;
            }
            // open the files...
            foreach (string filename in config.filenames)
            {
                var rawFile = openRawFile(filename);
                if (rawFile == null)
                {
                    continue;
                }
                Console.WriteLine("#INFO: The RAW file has data from {0} instruments", rawFile.InstrumentCount);
                rawFile.SelectMsData();
                var filters = rawFile.GetFilters();
                foreach (var f in filters)
                {
                    Console.WriteLine("#INFO: Available Filter: {0}", f.ToString());
                }
                var filter = rawFile.GetFilterFromString(config.filtertext);
                Console.WriteLine("#INFO: Selected Filter: {0}", filter.ToString());
                Console.WriteLine("#pkIdx\trtic\tmzic\ttic\tpeakarea\trt1st\trtLast");

                var scans = rawFile.GetFilteredScanEnumerator(filter);
                //double maxsn = 0;

                List<MzPeak> mzPeaks = new List<MzPeak>();

                int peakIndex = 0;
                int maxPeaksCount = 0;

                foreach (int scani in scans)
                {
                    var scan = rawFile.GetCentroidStream(scani, false);
                    var centroids = scan.GetCentroids();
                    var rt = rawFile.RetentionTimeFromScanNumber(scani);
                    var evt = rawFile.GetScanEventForScanNumber(scani);

                    int mzPeakIndex = 0;


                    foreach (var peak in centroids)
                    {
                        // does it meet intensity and s/n thresholds?
                        if (peak.SignalToNoise >= config.snr && peak.Intensity >= config.thresh)
                        {

                            //Console.WriteLine("{0}\t{1}\t{2}\t{3}\t{4}\t{5}", scani, rt, evt.MSOrder, peak.SignalToNoise, peak.Mass, peak.Intensity);

                            
                            // does it match something in the existing peak list?

                            while(mzPeakIndex < mzPeaks.Count && mzPeaks[mzPeakIndex].CompareMZ(peak.Mass, config.ppm) > 0)
                            {
                                // current mzPeak < this peak.Mass , so try next mzPeak
                                
                                // so, has the current peak been added to this time?
                                if (mzPeaks[mzPeakIndex].lastRt == rt)
                                {
                                    mzPeakIndex++; // yes, so skip to the next peak
                                }
                                else
                                { // no, so remove it // but first report 
                                    if (mzPeaks[mzPeakIndex].mzPeaks.Count >= config.minpeakpoints)
                                    {
                                        if (config.printall)
                                        {
                                            Console.WriteLine("PEAK_{0}:", peakIndex);

                                        }
                                        double rtic = 0;
                                        double mzic = 0;
                                        double tic = mzPeaks[mzPeakIndex].tic;
                                        // for peak area calculation:
                                        double lastrt = 0;
                                        double lastic = 0;
                                        double peakarea = 0;
                                        foreach (var mzPeak in mzPeaks[mzPeakIndex].mzPeaks)
                                        {
                                            rtic += mzPeak.rt * mzPeak.ic / tic;
                                            mzic += mzPeak.mz * mzPeak.ic / tic;
                                            if (config.printall)
                                            {
                                                Console.WriteLine("{0:F2}\t{1:F4}\t{2:F0}", mzPeak.rt, mzPeak.mz, mzPeak.ic);
                                            }

                                            //  peak area calculation:
                                            if(lastrt > 0){
                                                double drt = mzPeak.rt - lastrt;
                                                double avic = (mzPeak.ic + lastic)/2;
                                                peakarea += drt * avic * 60; // (minutes to seconds)
                                            }
                                            lastic = mzPeak.ic;
                                            lastrt = mzPeak.rt;
                                        }
                                        Console.WriteLine(
                                            "PEAK_{0}\t{1:F2}\t{2:F4}\t{3:F0}\t{4:F0}\t{5:F2}\t{6:F2}", 
                                            peakIndex, rtic, mzic, tic, peakarea,
                                             mzPeaks[mzPeakIndex].mzPeaks.First().rt, 
                                             mzPeaks[mzPeakIndex].mzPeaks.Last().rt);
                                        peakIndex++;
                                    }
                                    mzPeaks.RemoveAt(mzPeakIndex);

                                }
                            }
                            // if we match the current peak, append and move to next centroid
                            if (mzPeaks.Count == 0 || mzPeaks.Count - 1 < mzPeakIndex)
                            {
                                mzPeaks.Add(new MzPeak(rt, peak.Mass, peak.Intensity));
                            }
                            else if(mzPeaks[mzPeakIndex].CompareMZ(peak.Mass, config.ppm) == 0)
                            {
                                mzPeaks[mzPeakIndex].Add(rt, peak.Mass, peak.Intensity);
                            }
                            // if we're between current and last peak, insert new peak initialised on this centroid
                            else
                            {
                                // current mzPeak > this peak.Mass, so rewind and insert a new mzPeak
                                mzPeaks.Insert(mzPeakIndex, new MzPeak(rt, peak.Mass, peak.Intensity));
                                
                            }

                            if(mzPeaks.Count > maxPeaksCount)
                            {
                                maxPeaksCount = mzPeaks.Count;
                            }
                        }
                    }
                    //Console.WriteLine("{0}\t{1}\t{2}", scani, scan.BasePeakMass, scan.BasePeakIntensity);
                }
                
                //Console.WriteLine("# Maximum signal/noise ratio: {0}", maxsn);
                Console.WriteLine("# Maximum mzPeaks List count: {0}", maxPeaksCount);
                Console.WriteLine("# Total number of peaks: {0}", peakIndex);
            }
        }
        static void PrintHelp()
        {
            Console.WriteLine("You need to specify a filename.");
        }

        private static IRawDataPlus openRawFile(string filename)
        {

            if (!File.Exists(filename))
            {
                Console.WriteLine("#ERROR: The file doesn't exist in the specified location - {0}", filename);
                return null;
            }
            var rawFile = RawFileReaderAdapter.FileFactory(filename);

            if (rawFile.IsError)
            {
                Console.WriteLine("#ERROR: Error trying to read {0} - {1}:{2}", filename, rawFile.FileError.ErrorCode, rawFile.FileError.ErrorMessage);
                return null;
            }
            if (!rawFile.IsOpen)
            {
                Console.WriteLine("#ERROR: Unable to open the RAW file using the RawFileReader class!");
                return null;
            }
            if (rawFile.InAcquisition)
            {
                Console.WriteLine("!ERROR: Could not open raw file {0} - in acquisition", filename);
                return null;
            }
            if(!rawFile.HasMsData)
            {
                Console.WriteLine("!ERROR: No MS data in raw file {0}", filename);
                return null;
            }
            return rawFile;
        }

    }
}
